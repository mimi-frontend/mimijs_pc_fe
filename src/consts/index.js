export const YZM_LOGIN = 0
export const ACCOUNT_LOGIN = 1

// 优惠券适用商品
export const WARE_GOODS = {
  '0': '全部商品适用',
  '1': '指定商品适用',
  '2': '全部分类适用',
  '3': '全部商品标签适用',
  '4': '全部品牌适用'
}

export const ACTIVITY_TYPE = {
  "single_group": '团购',
  "full_minus": '满减',
  "full_discount": '满折',
  "full_gift": '满赠',
  "normal": '秒杀',
  "limited_time_sale": '限时特惠',
  "plus_price_buy": '换购',
  "member_preference": "会员限购"
}
