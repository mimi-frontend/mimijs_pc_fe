import SpModal from './src/index.vue'
import SpModalPlugin from './src/plugin'

export default SpModal

export { SpModalPlugin }
