import SpLoading from './src/index.vue'
import SpLoadingPlugin from './src/plugin'
import SpLoadingDirective from './src/directive'

export default SpLoading

export { SpLoadingPlugin, SpLoadingDirective }
