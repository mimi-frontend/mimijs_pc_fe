import SpFloatMenu from './src/index'
import SpFloatMenuItem from './src/item'
import SpFloatMenuInfo from './src/info'

export default SpFloatMenu

export {
  SpFloatMenu,
  SpFloatMenuItem,
  SpFloatMenuInfo
}
