import W0000 from './W0000'
import W0001 from './W0001'
import W0002 from './W0002'
import W0003 from './W0003'
import W0004 from './W0004'
import W0005 from './W0005'
import W0006 from './W0006'
import W0007 from './W0007'
import W0008 from './W0008'
import W0010 from './W0010'
import W0011 from './W0011'
import W0012 from './W0012'
import W0013 from './W0013'
import W0014 from './W0014'
import W0015 from './W0015'
import W0016 from './W0016'
import W0017 from './W0017'
import W0018 from './W0018'
import W0019 from './W0019'
import W0020 from './W0020'
import W0021 from './W0021'
import W0022 from './W0022'

export default {
  W0000,
  W0001,
  W0002,
  W0003,
  W0004,
  W0005,
  W0006,
  W0007,
  W0008,
  W0010,
  W0011,
  W0012,
  W0013,
  W0014,
  W0015,
  W0016,
  W0017,
  W0018,
  W0019,
  W0020,
  W0021,
  W0022,
}
