# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 3.13.2 (2023-12-19)

### [3.13.1](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.13.0...v3.13.1) (2023-06-05)

## [3.13.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.12.1...v3.13.0) (2023-06-01)

### [3.12.1](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.12.0...v3.12.1) (2023-05-24)

## 3.12.0 (2023-05-18)

## [3.11.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.10.1...v3.11.0) (2023-04-24)

### [3.10.1](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.10.0...v3.10.1) (2023-03-14)

## [3.10.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.9.0...v3.10.0) (2023-02-09)

## [3.9.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.8.0...v3.9.0) (2023-01-05)

## [3.8.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.7.0...v3.8.0) (2022-12-15)

## [3.7.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.6.1...v3.7.0) (2022-11-24)

## [3.6.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.4.0...v3.6.0) (2022-10-31)

## [3.5.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.4.0...v3.5.0) (2022-09-29)

## [3.4.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.3.0...v3.4.0) (2022-09-16)

## [3.3.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.2.1...v3.3.0) (2022-08-31)

### [3.2.1](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.2.0...v3.2.1) (2022-08-25)

## [3.2.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.1.0...v3.2.0) (2022-08-04)

## [3.1.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v3.0.0...v3.1.0) (2022-07-21)


### Features

* 3.1.0 ([ef0bff5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/ef0bff5862e674cba3e42f7f02d4ebb24efdee4b))

## [2.10.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v2.9.0...v2.10.0) (2022-06-23)


### Features

* 2.10.0 ([0099412](https://git.ishopex.cn/onex/ecshopx-newpc/commit/0099412d996328a8d91e654adfeb0e0cfcdac553))

## [2.9.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v2.8.0...v2.9.0) (2022-06-09)


### Features

* 2.9.0 ([484948c](https://git.ishopex.cn/onex/ecshopx-newpc/commit/484948ca8650d0858909e67fb408d9fcf4cf43c9))

## [2.8.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v2.7.0...v2.8.0) (2022-05-26)


### Features

* 2.8.0 ([9671a24](https://git.ishopex.cn/onex/ecshopx-newpc/commit/9671a24740c3692c534b644da377a96d6c9a693f))

## [2.7.0](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v2.6.3...v2.7.0) (2022-05-12)


### Features

* 2.7.0 ([db36624](https://git.ishopex.cn/onex/ecshopx-newpc/commit/db36624112ce32d8d73183b565d239b3bd390c9c))

### [2.6.3](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v2.6.2...v2.6.3) (2022-05-09)

### [2.6.2](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v2.6.1...v2.6.2) (2022-05-09)

### [1.1.1](https://git.ishopex.cn/onex/ecshopx-newpc/compare/v1.1.0...v1.1.1) (2021-05-13)


### Bug Fixes

* add changelog ([f71d9a0](https://git.ishopex.cn/onex/ecshopx-newpc/commit/f71d9a0230785d59ca9326538756df310197df3e))

## 1.1.0 (2021-05-13)


### Features

* 添加会员价 ([f2ee434](https://git.ishopex.cn/onex/ecshopx-newpc/commit/f2ee434f50739c4d75d123c7b8a631a6a0ee45ac))


### Bug Fixes

* bug修复 ([4ae80aa](https://git.ishopex.cn/onex/ecshopx-newpc/commit/4ae80aa99726076aeac0b9a91952b4bb43ca8bc3))
* merge ([38754a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/38754a51326cb1fb5b5dfa5ac5e99a921a39e6df))
* merge ([c1c7075](https://git.ishopex.cn/onex/ecshopx-newpc/commit/c1c70751953cdc5e33a732f43492516a2727f311))
* package.json ([389b447](https://git.ishopex.cn/onex/ecshopx-newpc/commit/389b4476bf467ec2b0b5986d0550c9cf4fcff227))
* por ([2e14978](https://git.ishopex.cn/onex/ecshopx-newpc/commit/2e14978d6ee02938d8491590ad7e892be1ffb006))
* port ([81137a0](https://git.ishopex.cn/onex/ecshopx-newpc/commit/81137a08cde3ba5aa7b932681be9ac66d9fafb1f))
* release version ([137e48a](https://git.ishopex.cn/onex/ecshopx-newpc/commit/137e48aa5dfb240d42ee14216f41f8b7931214ed))
* saas 独立域名 ([6aaefd2](https://git.ishopex.cn/onex/ecshopx-newpc/commit/6aaefd2cde30abfaac84f58533163a4b72e6829e))
* saas取消companyid ([9caac88](https://git.ishopex.cn/onex/ecshopx-newpc/commit/9caac881e7b73bbc98cb7a6ff3c883ae79e435cb))
* store commit ([fb04ec8](https://git.ishopex.cn/onex/ecshopx-newpc/commit/fb04ec82b18af6baac363d1dfbfb6566c656b464))
* 优化 ([572b09c](https://git.ishopex.cn/onex/ecshopx-newpc/commit/572b09cb69a4f860c86d247f88298271a1ef70cf))
* 会员价 ([c04ac5e](https://git.ishopex.cn/onex/ecshopx-newpc/commit/c04ac5e6546cdfd7d54dbd1aaa9b8c322af4efa2))
* 修改bug ([27be6e3](https://git.ishopex.cn/onex/ecshopx-newpc/commit/27be6e323fe46f6945dee0c9300f3227e8220165))
* 修改saas env变量 ([21367a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/21367a59f9532ee7c20b13e458433b57038d98a9))
* 修改UI ([628b85e](https://git.ishopex.cn/onex/ecshopx-newpc/commit/628b85ea4ac6e7c45119ddd835629b320e4e54ee))
* 修改购物车 ([bd158de](https://git.ishopex.cn/onex/ecshopx-newpc/commit/bd158de24e968a21e5d9ca4e8457636c996c023b))
* 注册优化 ([785a8ba](https://git.ishopex.cn/onex/ecshopx-newpc/commit/785a8ba837dfbec1524f10d1b5049d49c3949f65))
* 购物车优惠券选择 ([fdd4261](https://git.ishopex.cn/onex/ecshopx-newpc/commit/fdd42611d6299e367840cf99a4b481ea91cff8b9))

### 1.0.2 (2021-05-13)


### Bug Fixes

* bug修复 ([4ae80aa](https://git.ishopex.cn/onex/ecshopx-newpc/commit/4ae80aa99726076aeac0b9a91952b4bb43ca8bc3))
* merge ([38754a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/38754a51326cb1fb5b5dfa5ac5e99a921a39e6df))
* merge ([c1c7075](https://git.ishopex.cn/onex/ecshopx-newpc/commit/c1c70751953cdc5e33a732f43492516a2727f311))
* package.json ([389b447](https://git.ishopex.cn/onex/ecshopx-newpc/commit/389b4476bf467ec2b0b5986d0550c9cf4fcff227))
* por ([2e14978](https://git.ishopex.cn/onex/ecshopx-newpc/commit/2e14978d6ee02938d8491590ad7e892be1ffb006))
* port ([81137a0](https://git.ishopex.cn/onex/ecshopx-newpc/commit/81137a08cde3ba5aa7b932681be9ac66d9fafb1f))
* release version ([137e48a](https://git.ishopex.cn/onex/ecshopx-newpc/commit/137e48aa5dfb240d42ee14216f41f8b7931214ed))
* saas 独立域名 ([6aaefd2](https://git.ishopex.cn/onex/ecshopx-newpc/commit/6aaefd2cde30abfaac84f58533163a4b72e6829e))
* saas取消companyid ([9caac88](https://git.ishopex.cn/onex/ecshopx-newpc/commit/9caac881e7b73bbc98cb7a6ff3c883ae79e435cb))
* store commit ([fb04ec8](https://git.ishopex.cn/onex/ecshopx-newpc/commit/fb04ec82b18af6baac363d1dfbfb6566c656b464))
* 优化 ([572b09c](https://git.ishopex.cn/onex/ecshopx-newpc/commit/572b09cb69a4f860c86d247f88298271a1ef70cf))
* 修改bug ([27be6e3](https://git.ishopex.cn/onex/ecshopx-newpc/commit/27be6e323fe46f6945dee0c9300f3227e8220165))
* 修改saas env变量 ([21367a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/21367a59f9532ee7c20b13e458433b57038d98a9))
* 修改UI ([628b85e](https://git.ishopex.cn/onex/ecshopx-newpc/commit/628b85ea4ac6e7c45119ddd835629b320e4e54ee))
* 修改购物车 ([bd158de](https://git.ishopex.cn/onex/ecshopx-newpc/commit/bd158de24e968a21e5d9ca4e8457636c996c023b))
* 注册优化 ([785a8ba](https://git.ishopex.cn/onex/ecshopx-newpc/commit/785a8ba837dfbec1524f10d1b5049d49c3949f65))

### 1.0.1 (2021-05-13)


### Bug Fixes

* bug修复 ([4ae80aa](https://git.ishopex.cn/onex/ecshopx-newpc/commit/4ae80aa99726076aeac0b9a91952b4bb43ca8bc3))
* merge ([38754a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/38754a51326cb1fb5b5dfa5ac5e99a921a39e6df))
* merge ([c1c7075](https://git.ishopex.cn/onex/ecshopx-newpc/commit/c1c70751953cdc5e33a732f43492516a2727f311))
* package.json ([389b447](https://git.ishopex.cn/onex/ecshopx-newpc/commit/389b4476bf467ec2b0b5986d0550c9cf4fcff227))
* por ([2e14978](https://git.ishopex.cn/onex/ecshopx-newpc/commit/2e14978d6ee02938d8491590ad7e892be1ffb006))
* port ([81137a0](https://git.ishopex.cn/onex/ecshopx-newpc/commit/81137a08cde3ba5aa7b932681be9ac66d9fafb1f))
* release version ([137e48a](https://git.ishopex.cn/onex/ecshopx-newpc/commit/137e48aa5dfb240d42ee14216f41f8b7931214ed))
* saas 独立域名 ([6aaefd2](https://git.ishopex.cn/onex/ecshopx-newpc/commit/6aaefd2cde30abfaac84f58533163a4b72e6829e))
* saas取消companyid ([9caac88](https://git.ishopex.cn/onex/ecshopx-newpc/commit/9caac881e7b73bbc98cb7a6ff3c883ae79e435cb))
* store commit ([fb04ec8](https://git.ishopex.cn/onex/ecshopx-newpc/commit/fb04ec82b18af6baac363d1dfbfb6566c656b464))
* 优化 ([572b09c](https://git.ishopex.cn/onex/ecshopx-newpc/commit/572b09cb69a4f860c86d247f88298271a1ef70cf))
* 修改bug ([27be6e3](https://git.ishopex.cn/onex/ecshopx-newpc/commit/27be6e323fe46f6945dee0c9300f3227e8220165))
* 修改saas env变量 ([21367a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/21367a59f9532ee7c20b13e458433b57038d98a9))
* 修改UI ([628b85e](https://git.ishopex.cn/onex/ecshopx-newpc/commit/628b85ea4ac6e7c45119ddd835629b320e4e54ee))
* 修改购物车 ([bd158de](https://git.ishopex.cn/onex/ecshopx-newpc/commit/bd158de24e968a21e5d9ca4e8457636c996c023b))
* 注册优化 ([785a8ba](https://git.ishopex.cn/onex/ecshopx-newpc/commit/785a8ba837dfbec1524f10d1b5049d49c3949f65))

### 0.1.1 (2021-05-13)


### Bug Fixes

* bug修复 ([4ae80aa](https://git.ishopex.cn/onex/ecshopx-newpc/commit/4ae80aa99726076aeac0b9a91952b4bb43ca8bc3))
* merge ([38754a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/38754a51326cb1fb5b5dfa5ac5e99a921a39e6df))
* merge ([c1c7075](https://git.ishopex.cn/onex/ecshopx-newpc/commit/c1c70751953cdc5e33a732f43492516a2727f311))
* package.json ([389b447](https://git.ishopex.cn/onex/ecshopx-newpc/commit/389b4476bf467ec2b0b5986d0550c9cf4fcff227))
* por ([2e14978](https://git.ishopex.cn/onex/ecshopx-newpc/commit/2e14978d6ee02938d8491590ad7e892be1ffb006))
* port ([81137a0](https://git.ishopex.cn/onex/ecshopx-newpc/commit/81137a08cde3ba5aa7b932681be9ac66d9fafb1f))
* saas 独立域名 ([6aaefd2](https://git.ishopex.cn/onex/ecshopx-newpc/commit/6aaefd2cde30abfaac84f58533163a4b72e6829e))
* saas取消companyid ([9caac88](https://git.ishopex.cn/onex/ecshopx-newpc/commit/9caac881e7b73bbc98cb7a6ff3c883ae79e435cb))
* store commit ([fb04ec8](https://git.ishopex.cn/onex/ecshopx-newpc/commit/fb04ec82b18af6baac363d1dfbfb6566c656b464))
* 优化 ([572b09c](https://git.ishopex.cn/onex/ecshopx-newpc/commit/572b09cb69a4f860c86d247f88298271a1ef70cf))
* 修改bug ([27be6e3](https://git.ishopex.cn/onex/ecshopx-newpc/commit/27be6e323fe46f6945dee0c9300f3227e8220165))
* 修改saas env变量 ([21367a5](https://git.ishopex.cn/onex/ecshopx-newpc/commit/21367a59f9532ee7c20b13e458433b57038d98a9))
* 修改UI ([628b85e](https://git.ishopex.cn/onex/ecshopx-newpc/commit/628b85ea4ac6e7c45119ddd835629b320e4e54ee))
* 修改购物车 ([bd158de](https://git.ishopex.cn/onex/ecshopx-newpc/commit/bd158de24e968a21e5d9ca4e8457636c996c023b))
* 注册优化 ([785a8ba](https://git.ishopex.cn/onex/ecshopx-newpc/commit/785a8ba837dfbec1524f10d1b5049d49c3949f65))
